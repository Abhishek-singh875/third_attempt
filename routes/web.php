<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\auth999;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


// Route::middleware(['auth:sanctum', 'verified'])->get('/home', function () {
//     return view('home');
// })->name('home');

Route::get('flights', function () {
    return view('home');
})->middleware('auth:api');

Route::get('/log', function () {
    Log::channel('check1')->info('you are looged not successfully');
})->name('logcheck');




Route::get('data',[auth999::class,'check']);